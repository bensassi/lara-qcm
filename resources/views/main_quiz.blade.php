@extends('layouts.app')

@section('head')
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <script>
    window.Laravel =  <?php use App\Models\Answer;echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
  </script>
@endsection

@section('top_bar')
  <nav class="navbar navbar-default navbar-static-top">
    <div class="logo-main-block">
      <div class="container">

          <a href="{{ url('/') }}" title="{{config('app.name')}}">
            <img src="{{asset('/images/logo.png')}}" class="img-responsive" alt="{{config('app.name')}}">
          </a>

      </div>
    </div>
    <div class="nav-bar">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="navbar-header">

              @if($sujet)
                <h4 class="heading">{{$sujet->name}}</h4>
              @endif
            </div>
          </div>
          <div class="col-md-6">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
              <!-- Right Side Of Navbar -->
              <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                <li id="clock"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>
@endsection

@section('content')

<div class="container">
  @if ($auth)
    <div class="home-main-block">
      <?php
        // Pour voir si l'utilisateur a passé le test
        $users =  App\Models\Answer::where('subject_id',$sujet->id)->where('user_id',Auth::user()->id)->first();
        //Les questions
       $que =  App\Models\Question::where('subject_id',$sujet->id)->first();
      ?>

          @if(!empty($users))
              <div class="alert alert-danger">
                  Vous avez déjà fait le test! Essayez un autre QCM ou vous pouvez Supprimer le test précedent <a href="{{route('delete.qcmuser', $sujet->id)}}" class="btn btn-wave">Cliquer ici</a>
              </div>
          @else
              <div id="question_block" class="question-block">
                  <question :topic_id="{{$sujet->id}}" ></question>
              </div>
          @endif


       @if(empty($que))
      <div class="alert alert-danger">
            Il n'y a pas des question de ce sujet
      </div>
      @endif
    </div>
  @endif
</div>
@endsection

@section('scripts')
  <!-- jQuery 3 -->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/jquery.cookie.js')}}"></script>
  <script src="{{asset('js/jquery.countdown.js')}}"></script>

  @if(empty($users) && !empty($que))
   <script>
    var sujet_id = {{$sujet->id}};
    var timer = {{$sujet->timer}};
     $(document).ready(function() {
      function e(e) {
          (116 == (e.which || e.keyCode) || 82 == (e.which || e.keyCode)) && e.preventDefault()
      }
      setTimeout(function() {
          $(".myQuestion:first-child").addClass("active");
          $(".prebtn").attr("disabled", true);
      }, 2500), history.pushState(null, null, document.URL), window.addEventListener("popstate", function() {
          history.pushState(null, null, document.URL)
      }), $(document).on("keydown", e), setTimeout(function() {
          $(".nextbtn").click(function() {
              var e = $(".myQuestion.active");
              $(e).removeClass("active"), 0 == $(e).next().length ? (Cookies.remove("time"), Cookies.set("done", "Your Quiz is Over...!", {
                  expires: 1
              }), location.href = "{{$sujet->id}}/finish") : ($(e).next().addClass("active"), $(".myForm")[0].reset(),
              $(".prebtn").attr("disabled", false))
          }),
          $(".prebtn").click(function() {
              var e = $(".myQuestion.active");
              $(e).removeClass("active"),
              $(e).prev().addClass("active"), $(".myForm")[0].reset()
              $(".myQuestion:first-child").hasClass("active") ?  $(".prebtn").attr("disabled", true) :   $(".prebtn").attr("disabled", false);
          })
      }, 700);
      var i, o = (new Date).getTime() + 6e4 * timer;
      if (Cookies.get("time") && Cookies.get("sujet_id") == sujet_id) {
          i = Cookies.get("time");
          var t = o - i,
              n = o - t;
          $("#clock").countdown(n, {
              elapse: !0
          }).on("update.countdown", function(e) {
              var i = $(this);
              e.elapsed ? (Cookies.set("done", "Your Quiz is Over...!", {
                  expires: 1
              }), Cookies.remove("time"), location.href = "{{$sujet->id}}/finish") : i.html(e.strftime("<span>%H:%M:%S</span>"))
          })
      } else Cookies.set("time", o, {
          expires: 1
      }), Cookies.set("sujet_id", sujet_id, {
          expires: 1
      }), $("#clock").countdown(o, {
          elapse: !0
      }).on("update.countdown", function(e) {
          var i = $(this);
          e.elapsed ? (Cookies.set("done", "Your Quiz is Over...!", {
              expires: 1
          }), Cookies.remove("time"), location.href = "{{$sujet->id}}/finish") : i.html(e.strftime("<span>%H:%M:%S</span>"))
      })
  });
  </script>
  @else
  {{ "" }}
  @endif




@endsection
