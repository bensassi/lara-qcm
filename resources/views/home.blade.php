@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <script>
        window.Laravel =  <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
@endsection

@section('top_bar')
    <nav class="navbar navbar-default navbar-static-top">
        <div class="logo-main-block">
            <div class="container">
                     <a href="{{ url('/') }}" title="{{config('app.name')}}">
                        <img src="{{asset('/images/logo.png')}}" class="img-responsive" alt="{{config('app.name')}}">
                    </a>

            </div>
        </div>
        <div class="nav-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="navbar-header">
                            <!-- Branding Image -->

                                <a class="tt" title="Quick Quiz Home" href="{{url('/')}}"><h4 class="heading">{{config('app.name')}}</h4></a>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                            <!-- Right Side Of Navbar -->
                            <ul class="nav navbar-nav navbar-right">
                                <!-- Authentication Links -->
                                @guest
                                    <li><a href="{{ route('login') }}" title="Login">Login</a></li>
                                    <li><a href="{{ route('register') }}" title="Register">Register</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            @if ($auth->role == 'A')
                                                <li><a href="{{url('/admin')}}" title="Dashboard">Dashboard</a></li>
                                             @endif
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    Se déconnecter
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endguest

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
@endsection

@section('content')
    <div class="container">
        @if ($auth)
            <div class="quiz-main-block">
                <div class="row">
                    @if ($subjects)
                        @foreach ($subjects as $subject)
                            <div class="col-md-4">
                                <div class="subject-block">
                                    <div class="card blue-grey darken-1">
                                        <div class="card-content white-text">
                                            <span class="card-title" style="text-align: center;font-weight: 600;">{{$subject->name}}</span>
                                            <p title="{{$subject->description}}">{{str_limit($subject->description, 120)}}</p>
                                            <div class="row">
                                                <div class="col-xs-6 pad-0">
                                                    <ul class="subject-detail">
                                                        <li>Score <i class="fa fa-long-arrow-right"></i></li>
                                                        <li>Total Score <i class="fa fa-long-arrow-right"></i></li>
                                                        <li>Questions <i class="fa fa-long-arrow-right"></i></li>
                                                        <li>Temps<i class="fa fa-long-arrow-right"></i></li>
                                                     </ul>
                                                </div>
                                                <div class="col-xs-6">
                                                    <ul class="subject-detail right">
                                                        <li>{{$subject->score}}</li>
                                                        <li>
                                                            @php
                                                                $qu_count = 0;
                                                            @endphp
                                                            @foreach($questions as $question)
                                                                @if($question->subject_id == $subject->id)
                                                                    @php
                                                                        $qu_count++;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            {{$subject->score*$qu_count}}
                                                        </li>
                                                        <li>
                                                            {{$qu_count}}
                                                        </li>
                                                        <li>
                                                            {{$subject->timer}} minutes
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="card-action text-center">

                                            @if (Session::has('added'))
                                                <div class="alert alert-success sessionmodal">
                                                    {{session('added')}}
                                                </div>
                                            @elseif (Session::has('updated'))
                                                <div class="alert alert-info sessionmodal">
                                                    {{session('updated')}}
                                                </div>
                                            @elseif (Session::has('deleted'))
                                                <div class="alert alert-danger sessionmodal">
                                                    {{session('deleted')}}
                                                </div>
                                            @endif

                                                 <a href="{{route('start_quiz', ['id' => $subject->id])}}" class="btn btn-block" title="Start Quiz">Commencer le test </a>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        @endif
        @if (!$auth)
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="home-main-block">

                            <h1 class="main-block-heading text-center">{{config('app.name')}}</h1>

                        <blockquote>
                            Veuillez vous <a href="{{ route('login') }}">Connecter</a>  pour démarrer le <strong>QCM</strong>
                        </blockquote>
                    </div>
                </div>
            </div>
        @endif
    </div>


@endsection

@section('scripts')

    <script>
        $( document ).ready(function() {
            $('.sessionmodal').addClass("active");
            setTimeout(function() {
                $('.sessionmodal').removeClass("active");
            }, 4500);
        });
    </script>


@endsection
