@extends('layouts.app')

@section('head')
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <script>
    window.Laravel =  <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
  </script>
@endsection

@section('top_bar')
  <nav class="navbar navbar-default navbar-static-top">
    <div class="logo-main-block">
      <div class="container">
           <a href="{{ url('/') }}" title="{{config('app.name')}}">
            <img src="{{asset('/images/logo.png')}}" class="img-responsive" alt="{{config('app.name')}}">
          </a>
       </div>
    </div>
    <div class="nav-bar">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="navbar-header">
              <!-- Branding Image -->
                 <a title="Quick Quiz Home" class="tt" href="{{ url('/') }}"><h4 class="heading">{{config('app.name')}}</h4></a>
             </div>
          </div>
          <div class="col-md-6">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
              <!-- Right Side Of Navbar -->
              <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                  <li><a href="{{ route('login') }}" title="Login">S'identifier</a></li>
                  <li><a href="{{ route('register') }}" title="Register">Enregistrer</a></li>
                @else
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                      {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      @if ($auth->role == 'A')
                        <li><a href="{{url('/admin')}}" title="Dashboard">Tableau de bord</a></li>
                      @endif
                      <li>
                        <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        Se déconnecter
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                      </li>
                    </ul>
                 </li>
                @endguest
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>
@endsection

@section('content')
<div class="container">
  @if ($auth)
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="home-main-block">
          <div class="question-block">
            <h2 class="text-center main-block-heading">{{$subject->title}} Resultat</h2>
            <table class="table table-bordered result-table">
              <thead>
                <tr>
                  <th>Nombre questions</th>
                  <th>Score/qst</th>
                    <th>Mon Score</th>
                  <th>Score totale</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{$count_questions}}</td>

                  <td>{{$subject->score}}</td>
                    <td>
                        @php
                            $mark = 0;
                            $correct = collect();
                        @endphp
{{--                        @dump($answers,false)--}}
                        @foreach ($answers as $answer)
                            @if ($answer->correct_answer == $answer->user_answer)
                                @php
                                    $mark++;
                                @endphp
                            @endif
                        @endforeach
                        @php
                            $correct = $mark*$subject->score;
                        @endphp
                        {{$correct}}
                    </td>
                    {{-- score Total --}}
                  <td>{{$subject->score*$count_questions}}</td>
                </tr>
              </tbody>
            </table>
            <h2 class="text-center">
                Merci !
            </h2>
          </div>
        </div>
      </div>
    </div>
  @endif
</div>
@endsection

@section('scripts')
  <script>
    $(document).ready(function(){
      history.pushState(null, null, document.URL);
      window.addEventListener('popstate', function () {
        history.pushState(null, null, document.URL);
      });
    });
  </script>
@endsection


