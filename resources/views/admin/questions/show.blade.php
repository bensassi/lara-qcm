@extends('layouts.admin', [
  'page_header' => "Questions / {$subject->name} ",
  'dash' => '',
  'quiz' => '',
  'users' => '',
  'questions' => 'active',
  'top_re' => '',
  'all_re' => '',
  'sett' => ''
])

@section('content')
  <div class="margin-bottom">
    <button type="button" class="btn btn-wave" data-toggle="modal" data-target="#createModal">Ajouter une question</button>
   </div>
  <!-- Create Modal -->
  <div id="createModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter une question</h4>
        </div>
        {!! Form::open(['method' => 'POST', 'action' => 'QuestionsController@store', 'files' => true]) !!}
          <div class="modal-body">
            <div class="row">
              <div class="col-md-4">
                {!! Form::hidden('subject_id', $subject->id) !!}
                <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
                  {!! Form::label('question', 'Question') !!}
                  <span class="required">*</span>
                  {!! Form::textarea('question', null, ['class' => 'form-control ', 'placeholder' => 'Entrer la question', 'rows'=>'8', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('question') }}</small>
                </div>
              <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                  {!! Form::label('type', 'Type question') !!}
                  <span class="required">*</span>
                  {!! Form::select('type', array('unique'=>'Choix unique', 'multiple'=>'Choix multiple'),null, ['class' => 'form-control select2 select-type', 'required' => 'required', 'placeholder'=>'Sélectionner le type']) !!}
                  <small class="text-danger">{{ $errors->first('correct_answer') }}</small>
              </div>

                <div class="form-group{{ $errors->has('correct_answer') ? ' has-error' : '' }}" >
                      {!! Form::label('answer', 'La réponse correcte') !!}
                      <span class="required">*</span>
                      {!! Form::select('correct_answer[]', array('option_1'=>'Option 1', 'option_2'=>'Option 2', 'option_3'=>'Option 3', 'option_4'=>'Option 4'),null, ['class' => 'form-control select2 correct_answer_select', 'required' => 'required', 'placeholder'=>'Sélectionner la réponse correct']) !!}
                      <small class="text-danger">{{ $errors->first('correct_answer') }}</small>
                  </div>


              </div>
              <div class="col-md-4">
                <div class="form-group{{ $errors->has('option_1') ? ' has-error' : '' }}">
                  {!! Form::label('option_1', 'Option 1') !!}
                  <span class="required">*</span>
                  {!! Form::text('option_1', null, ['class' => 'form-control', 'placeholder' => 'Option 1', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('option_1') }}</small>
                </div>
                <div class="form-group{{ $errors->has('option_2') ? ' has-error' : '' }}">
                  {!! Form::label('option_2', 'Option 2') !!}
                  <span ></span>
                  {!! Form::text('option_2', null, ['class' => 'form-control', 'placeholder' => 'Option 2']) !!}
                  <small class="text-danger">{{ $errors->first('option_2') }}</small>
                </div>
                <div class="form-group{{ $errors->has('option_3') ? ' has-error' : '' }}">
                  {!! Form::label('option_3', 'Option 3') !!}
                  <span ></span>
                  {!! Form::text('option_3', null, ['class' => 'form-control', 'placeholder' => 'Option 3']) !!}
                  <small class="text-danger">{{ $errors->first('option_3') }}</small>
                </div>
                <div class="form-group{{ $errors->has('option_4') ? ' has-error' : '' }}">
                  {!! Form::label('option_4', 'Option 4') !!}
                  <span ></span>
                  {!! Form::text('option_4', null, ['class' => 'form-control', 'placeholder' => 'Option 4']) !!}
                  <small class="text-danger">{{ $errors->first('option_4') }}</small>
                </div>

              </div>

            </div>
          </div>
          <div class="modal-footer">
            <div class="btn-group pull-right">
              {!! Form::reset("Annuler", ['class' => 'btn btn-default']) !!}
              {!! Form::submit("Ajouter", ['class' => 'btn btn-wave']) !!}
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  <div class="box">
    <div class="box-body table-responsive">
      <table id="questions_table" class="table table-hover table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Question</th>
            <th>Option 1</th>
            <th>Option 2</th>
            <th>Option 3</th>
            <th>Option 4</th>
            <th>La réponse correcte</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          @if ($questions)
            @foreach ($questions as $key => $question)
              @php
                $answer = strtolower($question->correct_answer);
              @endphp
              <tr>
                <td>
                  {{$key+1}}
                </td>
                <td>{{$question->question}}</td>
                <td>{{$question->option_1}}</td>
                <td>{{$question->option_2}}</td>
                <td>{{$question->option_3}}</td>
                <td>{{$question->option_4}}</td>
                <td>{{$answer}}</td>
                <td>
                    <!-- Delete Button -->
                    <a type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#{{$question->id}}deleteModal"><i class="fa fa-close"></i> Delete</a>
                    <div id="{{$question->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                        <!-- Delete Modal -->
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <div class="delete-icon"></div>
                                </div>
                                <div class="modal-body text-center">
                                    <h4 class="modal-heading">Êtes-vous sûr ?</h4>
                                    <p>Voulez-vous vraiment supprimer ces enregistrements? Ce processus ne peut pas être annulé
                                        .</p>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::open(['method' => 'DELETE', 'action' => ['QuestionsController@destroy', $question->id]]) !!}
                                    {!! Form::reset("No", ['class' => 'btn btn-gray', 'data-dismiss' => 'modal']) !!}
                                    {!! Form::submit("Yes", ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                  </td>
              </tr>

            @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('.select-type').change(function(){

                 let value = $(this).val();

                $(".correct_answer_select").prop("multiple", value == "multiple" ? "multiple" : "");
                $(".correct_answer_select").prop("placeholder", (value == "multiple" ? "Sélectionner les réponses correctes" : "Sélectionner la réponses correcte")).blur();

            });

        });
    </script>
@endsection


