@extends('layouts.admin', [
  'page_header' => 'Les Question Par sujet',
  'dash' => '',
  'quiz' => '',
  'users' => '',
  'questions' => 'active',
  'top_re' => '',
  'all_re' => '',
  'sett' => ''
])

@section('content')
  <div class="row">
    @if ($subjects)
      @foreach ($subjects as $key => $subject)
        <div class="col-md-4">
          <div class="quiz-card">
            <h3 class="quiz-name">{{$subject->name}}</h3>

            <div class="row">
              <div class="col-xs-6 pad-0">
                <ul class="subject-detail">
                  <li>Score <i class="fa fa-long-arrow-right"></i></li>
                  <li>Total Marks <i class="fa fa-long-arrow-right"></i></li>
                  <li>Questions <i class="fa fa-long-arrow-right"></i></li>
                  <li>Total Temp <i class="fa fa-long-arrow-right"></i></li>
                </ul>
              </div>
              <div class="col-xs-6">
                <ul class="subject-detail right">
                  <li>{{$subject->score}}</li>
                  <li>
                    @php
                        $qu_count = 0;
                    @endphp
                    @foreach($questions as $question)
                      @if($question->subject_id == $subject->id)
                        @php
                          $qu_count++;
                        @endphp
                      @endif
                    @endforeach
                    {{$subject->score*$qu_count}}
                  </li>
{{--                    nombre des questions--}}
                  <li>
                    {{$qu_count}}
                  </li>
                  <li>
                    {{$subject->timer}} minutes
                  </li>
                </ul>
              </div>
            </div>
            <a href="{{route('questions.show', $subject->id)}}" class="btn btn-wave">Ajouter une question</a>
           </div>
        </div>
      @endforeach
    @endif
  </div>
@endsection
