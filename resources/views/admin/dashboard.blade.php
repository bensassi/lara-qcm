@extends('layouts.admin', [
  'page_header' => 'Tableau de bord',
  'dash' => 'active',
  'quiz' => '',
  'users' => '',
  'questions' => '',
  'top_re' => '',
  'all_re' => '',
  'sett' => ''
])

@section('content')
<!---->
  <div class="dashboard-block">
    <div class="row">
      <div class="col-md-12">
        <div class="row">

          <div class="col-md-4">
            <div class="small-box bg-red">
              <div class="inner">
                <h3>{{$quiz}}</h3>
                <p>Sujets</p>
              </div>
              <a href="{{url('/admin/subjects')}}" class="small-box-footer">
                  Plus infos <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="small-box bg-green">
              <div class="inner">
                <h3>{{$question}}</h3>
                <p>Questions</p>
              </div>
              <a href="{{url('/admin/questions')}}" class="small-box-footer">
                 Plus infos <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
           <div class="col-md-4">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$user}}</h3>
                        <p>
                            Utilisateurs
                        </p>
                    </div>

                    <a href="{{url('/admin/users')}}" class="small-box-footer">
                        Plus infos <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
