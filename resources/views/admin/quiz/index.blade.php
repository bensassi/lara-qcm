@extends('layouts.admin', [
  'page_header' => 'Sujets',
  'dash' => '',
  'quiz' => 'active',
  'users' => '',
  'questions' => '',
  'top_re' => '',
  'all_re' => '',
  'sett' => ''
])

@section('content')
  <div class="margin-bottom">
    <button type="button" class="btn btn-wave" data-toggle="modal" data-target="#createModal">Ajouter un sujet</button>
  </div>
  <!-- Create Modal -->
  <div id="createModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajouter un sujet</h4>
        </div>
        {!! Form::open(['method' => 'POST', 'action' => 'SubjectController@store']) !!}
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                  {!! Form::label('title', 'Titre') !!}
                  <span class="required">*</span>
                  {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Entrer le nom du sujet', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>
                <div class="form-group{{ $errors->has('per_q_mark') ? ' has-error' : '' }}">
                  {!! Form::label('per_q_mark', 'Score') !!}
                  <span class="required">*</span>
                  {!! Form::number('score', null, ['class' => 'form-control', 'placeholder' => 'Veillez entrer le score', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('score') }}</small>
                </div>
                <div class="form-group{{ $errors->has('timer') ? ' has-error' : '' }}">
                  {!! Form::label('timer', 'Temps (En Minutes)') !!}
                  {!! Form::number('timer', null, ['class' => 'form-control', 'placeholder' => 'Veuillez saisir la durée totale du questionnaire (en minutes)']) !!}
                  <small class="text-danger">{{ $errors->first('timer') }}</small>
                </div>



              <div class="form-group {{ $errors->has('show_ans') ? ' has-error' : '' }}">
                  <label for="">Activer Afficher la réponse : </label>
                 <input type="checkbox" class="toggle-input" name="show_ans" id="toggle2">
                 <label for="toggle2"></label>
                <br>
              </div>

              </div>

            </div>
          </div>
          <div class="modal-footer">
            <div class="btn-group pull-right">
              {!! Form::reset("Annuler", ['class' => 'btn btn-default']) !!}
              {!! Form::submit("Ajouter", ['class' => 'btn btn-wave']) !!}
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <div class="box">
    <div class="box-body table-responsive">
      <table id="search" class="table table-hover table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Titre Sujet</th>
            <th>Description</th>
            <th>Score</th>
            <th>Temps</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          @if ($subjects)
            @php($i = 1)
            @foreach ($subjects as $subject)
              <tr>
                <td>
                  {{$i}}
                  @php($i++)
                </td>
                <td>{{$subject->name}}</td>
                <td title="{{$subject->description}}">{{str_limit($subject->description, 50)}}</td>
                <td>{{$subject->score}}</td>
                <td>{{$subject->timer}} min</td>
                <td>
                  <!-- Edit Button -->
                  <a type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#{{$subject->id}}EditModal"><i class="fa fa-edit"></i> Modifier</a>
                  <!-- Delete Button -->
                  <a type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#{{$subject->id}}deleteModal"><i class="fa fa-close"></i> Supprimer</a>
                  <div id="{{$subject->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                    <!-- Delete Modal -->
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <div class="delete-icon"></div>
                        </div>
                        <div class="modal-body text-center">
                          <h4 class="modal-heading">Êtes-vous sûr ?</h4>
                          <p>Voulez-vous vraiment supprimer ces enregistrements? Ce processus ne peut pas être annulé.
                          </p>
                        </div>
                        <div class="modal-footer">
                          {!! Form::open(['method' => 'DELETE', 'action' => ['SubjectController@destroy', $subject->id]]) !!}
                            {!! Form::reset("No", ['class' => 'btn btn-gray', 'data-dismiss' => 'modal']) !!}
                            {!! Form::submit("Yes", ['class' => 'btn btn-danger']) !!}
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <!-- edit model -->
              <div id="{{$subject->id}}EditModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Modifier Sujet</h4>
                    </div>
                    {!! Form::model($subject, ['method' => 'PATCH', 'action' => ['SubjectController@update', $subject->id]]) !!}
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                              {!! Form::label('title', 'Nom Sujet') !!}
                              <span class="required">*</span>
                              {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'titre de sujet', 'required' => 'required']) !!}
                              <small class="text-danger">{{ $errors->first('title') }}</small>
                            </div>
                            <div class="form-group{{ $errors->has('per_q_mark') ? ' has-error' : '' }}">
                              {!! Form::label('per_q_mark', 'Score') !!}
                              <span class="required">*</span>
                              {!! Form::number('score', null, ['class' => 'form-control', 'placeholder' => 'Please Enter Per Question Mark', 'required' => 'required']) !!}
                              <small class="text-danger">{{ $errors->first('per_q_mark') }}</small>
                            </div>
                            <div class="form-group{{ $errors->has('timer') ? ' has-error' : '' }}">
                              {!! Form::label('timer', 'Temps (En minutes)') !!}
                              {!! Form::number('timer', null, ['class' => 'form-control', 'placeholder' => 'Please Enter Quiz Total Time (In Minutes)']) !!}
                              <small class="text-danger">{{ $errors->first('timer') }}</small>
                            </div>



                        </div>


                            </div>


                        </div>



                      <div class="modal-footer">
                        <div class="btn-group pull-right">
                          {!! Form::submit("Modifier", ['class' => 'btn btn-wave']) !!}
                        </div>
                      </div>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>
@endsection
@section('scripts')
<script type="text/javascript">


                    $(document).ready(function(){

                        $('.quizfp').change(function(){

                          if ($('.quizfp').is(':checked')){
                              $('#doabox').show('fast');
                          }else{
                              $('#doabox').hide('fast');
                          }


                        });

                    });



  $('#priceCheck').change(function(){
    alert('hi');
  });

  function showprice(id)
  {
    if ($('#toggle2'+id).is(':checked')){
      $('#doabox2'+id).show('fast');
    }else{

      $('#doabox2'+id).hide('fast');
    }
  }




</script>



@endsection

