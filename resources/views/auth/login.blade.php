@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
        window.Laravel =  <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
@endsection

@section('content')
    <div class="">
        <div class="container">
            @if (Session::has('error'))
                <div class="alert alert-danger sessionmodal">
                    {{session('error')}}
                </div>
            @endif
            <div class="login-page">
                <div class="logo">
                         <a href="{{url('/')}}" title="{{config('app.name')}}"><img src="{{asset('/images/logo.png')}}" class="login-logo img-responsive" alt="{{config('app.name')}}"></a>
                 </div>
                <h4 class="user-register-heading text-center">Identification</h4>

                 <form class="form login-form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Votre email" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Votre mot de passe" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="checkbox remember-me">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            </label>
                             Souvenir de moi
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-wave">
                            Login
                        </button>
                        <p class="messege text-center">N'est pas inscrit '? <a href="{{url('/register')}}" title="Create An Account">Créer un compte</a></p>
                    </div>
                    <div class="form-group text-center">
                        <a href="{{url('/password/reset')}}" title="Forgot Password">J'ai oublié le mot de passe </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $( document ).ready(function() {
                $('.sessionmodal').addClass("active");
                setTimeout(function() {
                    $('.sessionmodal').removeClass("active");
                }, 4500);
            });
        });
    </script>
@endsection
