@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
        window.Laravel =  <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
@endsection

@section('content')
    <div class="">
        <div class="container">
            <div class="login-page">
                <div class="logo">
                         <a href="{{url('/')}}" title="{{config('app.name')}}"><img src="{{asset('/images/logo.png')}}" class="img-responsive login-logo" alt="{{config('app.name')}}"></a>
                 </div>
                <h3 class="user-register-heading text-center">Enregister</h3>
                <form class="form login-form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Votre nom') !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Entrer votre nom']) !!}
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('email', 'Votre email') !!}
                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'ex: test@domain.tn']) !!}
                        <small class="text-danger">{{ $errors->first('email') }}</small>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        {!! Form::label('password', 'Votre mot de passe') !!}
                        {!! Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Entrer le mot de passe']) !!}
                        <small class="text-danger" style="color: red; background-color: #FFF;">{{ $errors->first('password') }}</small>
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        {!! Form::label('password_confirmation', 'Confirmation') !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Confirmer le mot de passe']) !!}
                        <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                    </div>
                    <div class="mr-t-20">
                        <button type="submit" class="btn btn-wave">Créer un compte</button>
                        <a href="{{url('/login')}}" class="text-center btn-block" title="Already Have Account">Vous avez deja un compte ?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
