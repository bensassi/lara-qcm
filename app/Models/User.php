<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'address', 'city', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * To verify the role
     * @return bool
     */
    public function is_admin() {
        if (Auth::check()) {
            if (Auth::user()->role == 'A') {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Ses réponses
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function answers() {
        return $this->hasOne('App\Models\Answer');
    }

    public function subject(){

    }



}
