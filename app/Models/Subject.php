<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'name', 'score', 'description', 'timer','show_response'
    ];


    public function answer(){
        return $this->hasOne('App\Model\Answer');
    }

    public function question(){
        return $this->hasOne('App\Model\Question');
    }

}
