<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\Array_;

class Question extends Model
{
    protected $fillable = [
        'subject_id',
        'question',
        'option_1',
        'option_2',
        'option_3',
        'option_4',
        'option_5',
        'option_6',
        'option_7',
        'correct_answer',
        'type'
     ];

    public function answers() {
        return $this->hasOne('App\Models\Answer');
    }

    public function subject() {
        return $this->belongsTo('App\Models\Subject');
    }


    public function setCorrectAnswerAttribute($value){
           $this->attributes['correct_answer']  = implode(',',$value);
    }
}
