<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Answer extends Model
{
    protected $fillable = [
        'subject_id', 'user_id', 'question_id', 'user_answer', 'correct_answer'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }

    public function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }

    public function answer()
    {
        return $this->belongsTo('App\Models\Answer');
    }

    public function setUserAnswerAttribute($value){
        $this->attributes['user_answer']  = is_array($value) ? implode(',',$value) : $value;
    }


}
