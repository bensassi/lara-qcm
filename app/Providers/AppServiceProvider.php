<?php

namespace App\Providers;

 use App\Models\Question;
 use App\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('*', function ($view)
        {
            $auth = Auth::user();
             $c_questions = Question::count();
            $view->with(['auth' => $auth, 'c_questions' => $c_questions]);
        });
    }
}
