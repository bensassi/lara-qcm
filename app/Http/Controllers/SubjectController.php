<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Answer;
use Illuminate\Support\Facades\Auth;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects= Subject::all();
        return view('admin.quiz.index', compact('subjects'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $input = $request->all();
        $request->validate([
          'name' => 'required|string',
          'score' => 'required',
        ]);


        if(isset($request->show_ans)){
          $input['show_ans'] = "1";
        }else{
          $input['show_ans'] = "0";
        }

            $subject = Subject::create($input);

       // $input['show_ans'] = $request->show_ans;
        //return Topic::create($input);
        return back()->with('added', 'Sujet a été ajouté avec succés');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

          'name' => 'required|string',
          'score' => 'required'

        ]);



          $subjetct = Subject::findOrFail($id);

        $subjetct->name = $request->name;
        $subjetct->description = $request->description;
        $subjetct->score = $request->per_q_mark;
        $subjetct->timer = $request->timer;

          if(isset($request->show_ans)){
              $subjetct->show_ans = 1;
          }else{
              $subjetct->show_ans = 0;
          }

        $subjetct->save();

          return back()->with('updated','Sujet a été mis à jours avec succés!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subject::findOrFail($id);
        $subject->delete();
        return back()->with('deleted', 'Sujet a été supprimé avec succeés');
    }

    public function deleteperquizsheet($id)
    {
        $findanswersheet = Answer::where('subject_id','=',$id)->where('user_id',Auth::user()->id)->get();

        if($findanswersheet->count()>0){
            foreach ($findanswersheet as $value) {
                $value->delete();
            }

            return back()->with('deleted','Qcm a été supprimé avec succés!');

        }
    }

}
