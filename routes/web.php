<?php


 use App\Http\Controllers\HomeController;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Subject;
use App\Models\User;
 use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'home');

Auth::routes();

Route::get('/faqs',function(){
    return view('faq');
})->name('faq.get');

Route::get('/home', 'HomeController@index');

Route::get('/admin/delete-qcm-per-user/{id}', 'SubjectController@deleteperquizsheet')->name('delete.qcmuser');


//Commencer le quiz (Partie frontend)
Route::get('start_quiz/{id}', function($id){

    $sujet = Subject::findOrFail($id);
    $answers = Answer::where('subject_id','=',$sujet->id)->first();
    $data = ['sujet','answers'];
    return view('main_quiz', compact($data));
})->name('start_quiz');


Route::resource('start_quiz/{id}/quiz', 'MainQuizController');

Route::group(['middleware'=> 'isadmin'], function(){



    Route::get('/admin', function(){

       // $user = User::where('role', '!=', 'A')->count();
        $user = User::all()->count();
        $question = Question::count();
        $quiz = Subject::count();
        $user_latest = User::where('id', '!=', Auth::id())->orderBy('created_at', 'desc')->get();
        $auth = Auth::user();

        return view('admin.dashboard', compact('user', 'question', 'quiz', 'user_latest','auth'));
        //remove the answer line comment
        // return view('admin.dashboard', compact('user', 'question', 'answer', 'quiz', 'user_latest'));

    });

    //Subjects
    Route::resource('/admin/subjects', 'SubjectController');

    //Les Questions
    Route::resource('/admin/questions', 'QuestionsController');
});

// la fin de QCM
Route::get('start_quiz/{id}/finish', function($id){
    $auth = Auth::user();
    $subject = Subject::findOrFail($id);
    $questions = Question::where('subject_id', $id)->get();
    $count_questions = $questions->count();
    $answers = Answer::where('user_id',$auth->id)
        ->where('subject_id',$id)->get();



    if($count_questions != $answers->count()){
        foreach($questions as $que){
            $a = false;
            foreach($answers as $ans){
                if($que->id == $ans->question_id){
                    $a = true;
                }
            }
            if($a == false){
                Answer::create([
                    'subject_id' => $id,
                    'user_id' => $auth->id,
                    'question_id' => $que->id,
                    'user_answer' => 0,
                    'correct_answer' => $que->correct_answer
                ]);
            }
        }
    }


      $ans= Answer::all();
     $q= Question::all();


    return view('finish', compact( 'q','subject', 'count_questions','ans','answers'));
});

Route::resource('/admin/users', 'UsersController');


