/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.24 : Database - lara_qcm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `answers` */

DROP TABLE IF EXISTS `answers`;

CREATE TABLE `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `correct_answer` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_answer` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `answers_subject_id_foreign` (`subject_id`),
  CONSTRAINT `answers_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `answers` */

insert  into `answers`(`id`,`subject_id`,`user_id`,`question_id`,`correct_answer`,`user_answer`,`created_at`,`updated_at`) values 
(57,3,6,7,'option_1,option_2','option_4','2020-12-17 23:09:55','2020-12-17 23:09:55'),
(58,3,6,8,'option_4','option_4','2020-12-17 23:09:57','2020-12-17 23:09:57'),
(59,3,6,2,'option_2','option_4','2020-12-17 23:09:59','2020-12-17 23:09:59'),
(60,3,6,3,'option_3','option_4','2020-12-17 23:10:01','2020-12-17 23:10:01'),
(61,3,6,9,'option_1','','2020-12-17 23:10:04','2020-12-17 23:10:04');

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1),
(4,'2020_12_16_135404_create_subjects_table',1),
(5,'2020_12_16_135923_create_questions_table',1),
(6,'2020_12_16_140508_create_answers_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `questions` */

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subject_id` int(10) unsigned NOT NULL,
  `option_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'unique' COMMENT 'Choix uniques ou multiple (unique ou multi)',
  `correct_answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questions_subject_id_foreign` (`subject_id`),
  CONSTRAINT `questions_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `questions` */

insert  into `questions`(`id`,`subject_id`,`option_1`,`option_2`,`option_3`,`option_4`,`option_5`,`option_6`,`option_7`,`question`,`type`,`correct_answer`,`created_at`,`updated_at`) values 
(1,2,'h1','h2','h3','h4',NULL,NULL,NULL,'Titre 1 c\'est : ','unique','option_1','2020-12-16 18:16:48','2020-12-16 18:16:48'),
(2,3,'Personal Home Page','Hypertext Preprocessor','Pretext Hypertext Processor','Preprocessor Home Page',NULL,NULL,NULL,'Que signifie PHP','unique','option_2','2020-12-17 08:13:15','2020-12-17 08:13:15'),
(3,3,'.html','.xml','.php','.ph',NULL,NULL,NULL,'Les fichiers PHP ont l’extension …. ?','unique','option_3','2020-12-17 08:22:53','2020-12-17 08:22:53'),
(4,2,'Apple Inc.','IBM Corporation','World Wide Web Consortium','Microsoft Corporation',NULL,NULL,NULL,'Quelle organisation définit les standards Web','unique','option_3','2020-12-17 15:20:51','2020-12-17 15:20:51'),
(5,2,'Langage de programmation','Langage POO','Langage de haut niveau','Langage de balisage',NULL,NULL,NULL,'HTML est considéré comme ______','unique','option_4','2020-12-17 15:21:43','2020-12-17 15:21:43'),
(6,2,'Balises définis par l’utilisateur','Balises prédéfinis','Balises fixes définis par le langage','Balises uniquement pour les liens',NULL,NULL,NULL,'HTML utilise des ______','unique','option_3','2020-12-17 15:22:48','2020-12-17 15:22:48'),
(7,3,'gfg','fgfgfgf','gfgf','gfgf',NULL,NULL,NULL,'gfgf','multiple','option_1,option_2','2020-12-17 19:41:39','2020-12-17 19:41:39'),
(8,3,'hh','tt','ff','bb',NULL,NULL,NULL,'test','unique','option_4','2020-12-17 19:42:33','2020-12-17 19:42:33'),
(9,3,'option 1111',NULL,NULL,NULL,NULL,NULL,NULL,'Une question avec une seule option','multiple','option_1','2020-12-17 22:57:10','2020-12-17 22:57:10');

/*Table structure for table `subjects` */

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `timer` text COLLATE utf8mb4_unicode_ci,
  `score` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `subjects` */

insert  into `subjects`(`id`,`name`,`description`,`timer`,`score`,`created_at`,`updated_at`) values 
(2,'Test Html',NULL,'10','4','2020-12-16 17:43:13','2020-12-16 17:43:13'),
(3,'Test PHP',NULL,'3','6','2020-12-17 08:09:36','2020-12-17 08:09:36');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` char(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`phone`,`address`,`city`,`role`,`remember_token`,`created_at`,`updated_at`) values 
(3,'abdeli','abdeli@gmail.com',NULL,'$2y$10$7rtMX/lIJ0iuoUeAciwQc.AE25JT.Es88ZLdeGiGJZOszIepmLvHq',NULL,'kalaat Andalous\r\nariana','tunis','A','S2rBq1Yp7Na2Gks5c6uEv0n4QxPYtgB68uLDD18CytBjj1PAzd4sQoxoDewN','2020-12-17 07:18:39','2020-12-17 07:24:34'),
(4,'Ben sassi Mohamed Ridha','bensassiridha1@gmail.com',NULL,'$2y$10$KHtYqeKiRspAvSFX1x6DPeO61N4hjTXabVpDN0mUcdexhKzRxy6pK','92702009','kalaat Andalous\r\nariana','Ariana','A',NULL,'2020-12-17 07:22:36','2020-12-17 07:22:36'),
(5,'chaaben fahmi','test@domai.com',NULL,'$2y$10$DrcKfEpVu7IEywrvUjvYK.3mIFu4tHSicBQifdsomP9tyHV8FBrlu','5478787878','kalaat Andalous','beja','U',NULL,'2020-12-17 08:08:21','2020-12-17 08:08:21'),
(6,'laratech','laratech@laratech.com',NULL,'$2y$10$I4D7ohh.ZdJ3fNaA6C6KkOE84Bxf3pcrV.Pz.WXpydvQYcxU3XgwO','92702055','kalaat Andalous\r\nariana','Ariana','A',NULL,'2020-12-17 15:18:14','2020-12-17 15:18:14');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
